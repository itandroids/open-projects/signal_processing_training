x = [0; 0; 0];
v = [1; 0; 0.1];
T = 1 / 30;
Q = diag([0.4, 0.4, 0.1]);
Qest = diag([0, 0, 0]);

numSamples = 1000;
actual = zeros(numSamples, 3);
estimate = zeros(numSamples, 3);
for i=2:numSamples
    actual(i, :) = sampleMotionModel(actual(i-1, :)', v, T, Q)';
    estimate(i, :) = sampleMotionModel(estimate(i-1, :)', v, T, Qest)';
end

plot(actual(:, 1), actual(:, 2), 'LineWidth', 2);
hold on;
plot(estimate(:, 1), estimate(:, 2), 'r', 'LineWidth', 2);
axis equal;
xlabel('X (m)', 'FontSize', 14)
ylabel('Y (m)', 'FontSize', 14)
grid on
set(gca, 'FontSize', 14)
legend('Actual', 'Estimate');