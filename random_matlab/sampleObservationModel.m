function z = sampleObservationModel(x, l, R)

delta = l - x(1:2,1);
d = sqrt(delta(1)^2 + delta(2)^2);
theta = atan2(delta(2), delta(1)) - x(3);

z = [d; theta];
z = mvnrnd(z, R);

end