function piValue = monteCarloPi(numPoints)

x = rand(numPoints, 2);

y = sqrt(x(:,1).^2 + x(:,2).^2) <= 1;

plot(x(y,1), x(y,2), '.b')
hold on;
plot(x(~y,1), x(~y,2), '.r')
set(gca, 'FontSize', 14);

piValue = 4 * sum(y) / numPoints;

end