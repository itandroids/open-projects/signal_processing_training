x = [0; 0; 0];
l = [1; 1];
R = diag([0.01, 0.1]);

numSamples = 10000;
samples = zeros(numSamples, 2);
for i=1:numSamples
    samples(i, :) = sampleObservationModel(x, l, R)';
end

xSamples = x(1) + samples(:, 1) .* cos(samples(:, 2) + x(3));
ySamples = x(2) + samples(:, 1) .* sin(samples(:, 2) + x(3));
plot(xSamples, ySamples, '.');
hold on;
plot(x(1), x(2), '*r');
axis([-1 2 -1 2])
grid on
xlabel('X (m)', 'FontSize', 14)
ylabel('Y (m)', 'FontSize', 14)
set(gca, 'FontSize', 14)

mean(samples)
std(samples)