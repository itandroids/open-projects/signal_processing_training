x = [0; 0; 0];
v = [1; 1; 0];
T = 1;
Q = diag([0.01, 0.01, 1]);

numSamples = 10000;
samples = zeros(numSamples, 3);
for i=1:numSamples
    samples(i, :) = sampleMotionModel(x, v, T, Q)';
end

plot(samples(:, 1), samples(:, 2), '.');
hold on;
plot(x(1), x(2), '*r');
axis([-1 2 -1 2])
grid on
xlabel('X (m)', 'FontSize', 14)
ylabel('Y (m)', 'FontSize', 14)
set(gca, 'FontSize', 14)

figure;
plot(samples(:, 3), 'LineWidth', 2)
grid on
xlabel('Sample (-)', 'FontSize', 14)
ylabel('\psi (rad)', 'FontSize', 14)
set(gca, 'FontSize', 14)

mean(samples)
std(samples)