function xNew = sampleMotionModel(x, v, T, Q)

v = mvnrnd(v, Q);

xNew = x;
if abs(v(3)) < 1e-3
    xNew(1) = x(1) + v(1) * T * cos(x(3) + v(3) * T / 2) -...
        v(2) * T * sin(x(3) + v(3) * T / 2);
    xNew(2) = x(2) + v(1) * T * sin(x(3) + v(3) * T / 2) +...
        v(2) * T * cos(x(3) + v(3) * T / 2);
    xNew(3) = x(3) + v(3) * T;
else
    xNew(1) = x(1) + (2 * v(1) / v(3)) * cos(x(3) + v(3) * T / 2) * sin(v(3) * T / 2) -...
        (2 * v(2) / v(3)) * sin(x(3) + v(3) * T / 2) * sin(v(3) * T / 2);
    xNew(2) = x(2) + (2 * v(1) / v(3)) * sin(x(3) + v(3) * T / 2) * sin(v(3) * T / 2) +...
        (2 * v(2) / v(3)) * cos(x(3) + v(3) * T / 2) * sin(v(3) * T / 2);
    xNew(3) = x(3) + v(3) * T;
end

end