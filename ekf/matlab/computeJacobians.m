syms x y psi v w T
%% When w is low
fxwLow = x + v * T * cos(psi + w * T / 2);
fywLow = y + v * T * sin(psi + w * T / 2);
fpsi = psi + w * T;

fwLow = [fxwLow; fywLow; fpsi];

AwLow = simplify(jacobian(fwLow, [x y psi]))
BwLow = simplify(jacobian(fwLow, [v w]))

%% When w is high
fxwHigh = x + 2 * (v / w) * cos(psi + w * T / 2) * sin(w * T / 2);
fywHigh = y + 2 * (v / w) * sin(psi + w * T / 2) * sin(w * T / 2);
fpsi = psi + w * T;

fwHigh = [fxwHigh; fywHigh; fpsi];

AwHigh = simplify(jacobian(fwHigh, [x y psi]))
BwHigh = simplify(jacobian(fwHigh, [v w]))
