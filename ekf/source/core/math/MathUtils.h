//
// Created by mmaximo on 3/24/18.
//

#ifndef EKF_MATHUTILS_H
#define EKF_MATHUTILS_H

namespace math {

class MathUtils {
public:
    static double normalizeAngle(double angle);
};

}

#endif //EKF_MATHUTILS_H
