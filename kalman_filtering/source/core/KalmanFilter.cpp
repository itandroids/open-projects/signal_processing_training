//
// Created by mmaximo on 3/16/18.
//

#include "KalmanFilter.h"

//namespace signal_processing {

KalmanFilter::KalmanFilter(const Eigen::MatrixXd &A, const Eigen::MatrixXd &B,
                                              const Eigen::MatrixXd &C, const Eigen::MatrixXd &Q,
                                              const Eigen::MatrixXd &R) : A(A), B(B), C(C), Q(Q), R(R) {
    x.resize(A.rows());
    P.resize(A.rows(), B.cols());
    reset();
}

void KalmanFilter::reset() {
    x.setZero();
    P.setIdentity();
    P *= 1.0e5;
}


void KalmanFilter::reset(const Eigen::VectorXd &x0, const Eigen::MatrixXd &P0) {
    x = x0;
    P = P0;
}


const Eigen::VectorXd &KalmanFilter::predict(const Eigen::VectorXd &u) {
    x = A * x + B * u;
    P = A * P * A.transpose() + Q;
    return x;
}

const Eigen::VectorXd &KalmanFilter::filter(const Eigen::VectorXd &z) {
    K = P * C.transpose() * (C * P * C.transpose() + R).inverse();
    x = x + K * (z - C * x);
    P = P - K * C * P;
    return x;
}

const Eigen::VectorXd &KalmanFilter::getEstimate() {
    return x;
}

const Eigen::MatrixXd &KalmanFilter::getCovariance() {
    return P;
}

//}