//
// Created by mmaximo on 3/16/18.
//

#include "Robot2DLocalizer.h"

Robot2DLocalizer::Robot2DLocalizer(double sigmaMotion, double sigmaGPS, double T) {
    Eigen::MatrixXd A(2, 2);
    A << 1.0, 0.0,
            0.0, 1.0;
    Eigen::MatrixXd B(2, 2);
    B << T, 0.0,
            0.0, T;
    Eigen::MatrixXd C(2, 2);
    C << 1.0, 0.0,
            0.0, 1.0;
    Eigen::MatrixXd M(2, 2);
    M << sigmaMotion * sigmaMotion, 0.0,
            0.0, sigmaMotion * sigmaMotion;
    Eigen::MatrixXd Q = B * M * B.transpose();
    Eigen::MatrixXd R(2, 2);
    R << sigmaGPS * sigmaGPS, 0.0,
            0.0, sigmaGPS * sigmaGPS;
    kalmanFilter = std::make_shared<KalmanFilter>(A, B, C, Q, R);
}

void Robot2DLocalizer::reset(const math::Vector2d &resetPosition, const Eigen::MatrixXd &resetCovariance) {
    Eigen::VectorXd x0(2);
    x0 << resetPosition.x, resetPosition.y;
    kalmanFilter->reset(x0, resetCovariance);
}


const math::Vector2d &Robot2DLocalizer::predict(const math::Vector2d &command) {
    Eigen::VectorXd u(2);
    u << command.x, command.y;
    Eigen::VectorXd x = kalmanFilter->predict(u);
    position.x = x(0);
    position.y = x(1);
    return position;
}

const math::Vector2d &Robot2DLocalizer::filter(const math::Vector2d &observation) {
    Eigen::VectorXd z(2);
    z << observation.x, observation.y;
    Eigen::VectorXd x = kalmanFilter->filter(z);
    position.x = x(0);
    position.y = x(1);
    return position;
}

const math::Vector2d &Robot2DLocalizer::getPosition() const {
    return position;
}

const Eigen::MatrixXd &Robot2DLocalizer::getCovariance() const {
    return kalmanFilter->getCovariance();
}
