//
// Created by mmaximo on 5/5/18.
//

#ifndef PARTICLE_FILTER_PARTICLEFILTERLOCALIZER_H
#define PARTICLE_FILTER_PARTICLEFILTERLOCALIZER_H

#include "math/Pose2D.h"
#include "LandmarkObservation.h"
#include "OdometryModel.h"
#include "ObservationModel.h"
#include "Resetter.h"

struct ParticleFilterLocalizerParams {
    double sigmaVx;
    double sigmaVy;
    double sigmaVpsi;
    double sigmaDistance;
    double sigmaBearing;
    int numParticles;
    double alphaSlow;
    double alphaFast;

    ParticleFilterLocalizerParams();

    static ParticleFilterLocalizerParams getDefaultParams();
};

/**
 * Represents a robot soccer localizer using Augmented Monte Carlo Localization.
 */
class ParticleFilterLocalizer {
public:
    /**
     * Constructs a robot soccer localizer.
     *
     * @param params localizer parameters.
     * @param fieldDescription description of the soccer field.
     */
    ParticleFilterLocalizer(const ParticleFilterLocalizerParams &params, FieldDescription &fieldDescription);

    /**
     * Destructs a robot soccer localizer.
     */
    ~ParticleFilterLocalizer();

    /**
     * Resets the localizer around an estimate.
     *
     * @param pose robot's pose estimate.
     * @param sigmaPose standard deviation vector for x, y, and psi directions.
     */
    void reset(math::Pose2D &pose, math::Pose2D &sigmaPose);

    /**
     * Executes a prediction step.
     *
     * @param elapsedTime time elapsed since last prediction.
     * @param velocity robot velocity.
     */
    void predict(double elapsedTime, const math::Pose2D &velocity);

    /**
     * Executes a filtering step.
     *
     * @param goalPostObservations goal post observations.
     * @param flagObservations flag observations.
     */
    void filter(const std::vector<GoalPostObservation> &goalPostObservations,
                const std::vector<FlagObservation> &flagObservations);

    /**
     * Obtains the robot's estimate.
     *
     * @return robot's estimate.
     */
    const math::Pose2D &getEstimate() const;

    /**
     * Obtains the filter's particle set.
     *
     * @return particle set.
     */
    const math::Pose2D *getParticles() const;

    /**
     * Obtains the localizer's parameters.
     *
     * @return localizer's parameters.
     */
    const ParticleFilterLocalizerParams &getParams() const;

private:

    ParticleFilterLocalizerParams params;
    OdometryModel odometryModel;
    ObservationModel observationModel;
    Resetter resetter;
    FieldDescription &fieldDescription;
    math::Pose2D *particles; /// particle set
    math::Pose2D *newParticles; /// auxiliary buffer used in resampling
    double *weights; /// particles' weights
    int *resampledIndices; /// resampled indices during resampling
    math::Pose2D estimate; /// robot's estimate
    double slowObservationsQuality; /// filtered observations quality using alphaSlow
    double fastObservationsQuality; /// filtered observations quality using alphaFast

    /// Updates weights
    void updateWeights(const std::vector<GoalPostObservation> &goalPostObservations,
                       const std::vector<FlagObservation> &flagObservations);

    /// Resamples particles according to their weights
    void resample();

    /// Computes the robot pose estimate given the current particle set
    void computeEstimate();

    /// Resets particles using current observations
    void sensorReset(const std::vector<GoalPostObservation> &goalPostObservations,
                     const std::vector<FlagObservation> &flagObservations);
};


#endif //PARTICLE_FILTER_PARTICLEFILTERLOCALIZER_H
