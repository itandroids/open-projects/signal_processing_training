/*
 * FieldDescription.cpp
 *
 *  Created on: Sep 6, 2015
 *      Author: mmaximo
 */

#include "FieldDescription.h"

const double FieldDescription::FIELD_LENGTH = 30.0;
const double FieldDescription::FIELD_WIDTH = 20.0;
const double FieldDescription::SOCCER_2D_FIELD_LENGTH = 105.0;
const double FieldDescription::SOCCER_2D_FIELD_WIDTH = 68.0;
const double FieldDescription::GOAL_AREA_LENGTH = 0.6;
const double FieldDescription::GOAL_AREA_WIDTH = 2.1;
const double FieldDescription::PENALTY_AREA_LENGTH = 1.8;

const double FieldDescription::GOAL_POST_HEIGHT = 0.0;
//const double FieldDescription::GOAL_POST_HEIGHT = 0.8;
const double FieldDescription::CENTER_CIRCLE_RADIUS = 2.0;

using math::Vector2;

FieldDescription::FieldDescription() {
    //default playside
    currentPlaySide = PlaySide::LEFT;

    //left side
    flagsPositionsLeft[FlagType::F1L] = Vector2<double>(-FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0);
    flagsPositionsLeft[FlagType::F2L] = Vector2<double>(-FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0);
    flagsPositionsLeft[FlagType::F1R] = Vector2<double>(FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0);
    flagsPositionsLeft[FlagType::F2R] = Vector2<double>(FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0);

    goalPostsPositionsLeft[GoalPostType::G1L] = Vector2<double>(-FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0);
    goalPostsPositionsLeft[GoalPostType::G2L] = Vector2<double>(-FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0);
    goalPostsPositionsLeft[GoalPostType::G1R] = Vector2<double>(FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0);
    goalPostsPositionsLeft[GoalPostType::G2R] = Vector2<double>(FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0);

    //right side
    flagsPositionsRight[FlagType::F1L] = Vector2<double>(FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0);
    flagsPositionsRight[FlagType::F2L] = Vector2<double>(FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0);
    flagsPositionsRight[FlagType::F1R] = Vector2<double>(-FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0);
    flagsPositionsRight[FlagType::F2R] = Vector2<double>(-FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0);

    goalPostsPositionsRight[GoalPostType::G1L] = Vector2<double>(FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0);
    goalPostsPositionsRight[GoalPostType::G2L] = Vector2<double>(FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0);
    goalPostsPositionsRight[GoalPostType::G1R] = Vector2<double>(-FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0);
    goalPostsPositionsRight[GoalPostType::G2R] = Vector2<double>(-FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0);
}

FieldDescription::~FieldDescription() {
}

void FieldDescription::update(PlaySide::PLAY_SIDE playSide) {
    currentPlaySide = playSide;
}

const Vector2<double> &FieldDescription::getFlagKnownPosition(
        FlagType::FLAG_TYPE flagType) {
    if (currentPlaySide == PlaySide::LEFT)
        return flagsPositionsLeft[flagType]; // Return flag's position

    else
        return flagsPositionsRight[flagType];
}

const Vector2<double> &FieldDescription::getGoalPostKnownPosition(
        GoalPostType::GOAL_POST_TYPE goalPostType) {
    if (currentPlaySide == PlaySide::LEFT)
        return goalPostsPositionsLeft[goalPostType]; // Return goal post's position
    else
        return goalPostsPositionsRight[goalPostType];
}

bool FieldDescription::isPointWithinSoccerField(math::Vector2<double> point) {
    return (point.x >= -FIELD_LENGTH / 2.0 && point.x <= FIELD_LENGTH / 2.0 && point.y >= -FIELD_WIDTH / 2.0 &&
            point.y <= FIELD_WIDTH / 2.0);
}
