//
// Created by mmaximo on 5/5/18.
//

#ifndef PARTICLE_FILTER_RESETTER_H
#define PARTICLE_FILTER_RESETTER_H

#include "FieldDescription.h"
#include "math/Pose2D.h"
#include "LandmarkObservation.h"
#include "math/Vector2.h"

/**
 * Represents a resetter for a robot soccer particle filter localizer.
 */
class Resetter {
public:
    /**
     * Constructs a resetter for a robot soccer particle filter localizer.
     *
     * @param fieldDescription description of the soccer field.
     * @param sigmaDistance standard deviation of distance observations.
     * @param sigmaBearing standard deviation of bearing observations.
     */
    Resetter(FieldDescription &fieldDescription, double sigmaDistance, double sigmaBearing);

    /**
     * Reset particle set around an estimate.
     *
     * @param estimate robot's pose estimate.
     * @param sigma standard deviation vector for x, y, and psi directions.
     * @param particles new particle set after reset.
     * @param numParticles number of particles.
     */
    void resetParticleSetAroundEstimate(const math::Pose2D &estimate, const math::Pose2D &sigma,
                                        math::Pose2D particles[], int numParticles);

    void
    sensorReset(math::Pose2D *particles, int numParticles, const std::vector<GoalPostObservation> &goalPostObservations,
                const std::vector<FlagObservation> &flagObservations, double resetProbability);

private:
    FieldDescription &fieldDescription;
    double sigmaDistance;
    double sigmaBearing;

    void resetWithTwoLandmarks(const LandmarkObservation &observation1,
                               const LandmarkObservation &observation2,
                               const math::Vector2<double> &knownPosition1,
                               const math::Vector2<double> &knownPosition2, math::Pose2D &particle);

    bool computeTwoCirclesIntersection(const math::Vector2<double> &center1, double radius1, const math::Vector2<double> &center2,
                                       double radius2,
                                       math::Vector2<double> &intersection1, math::Vector2<double> &intersection2);
};


#endif //PARTICLE_FILTER_RESETTER_H
