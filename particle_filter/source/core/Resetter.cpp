//
// Created by mmaximo on 5/5/18.
//

#include "Resetter.h"
#include "math/RandomUtils.h"
#include "math/MathUtils.h"

Resetter::Resetter(FieldDescription &fieldDescription, double sigmaDistance, double sigmaBearing)
        : fieldDescription(fieldDescription), sigmaDistance(sigmaDistance), sigmaBearing(sigmaBearing) {

}

void
Resetter::resetParticleSetAroundEstimate(const math::Pose2D &estimate, const math::Pose2D &sigma,
                                         math::Pose2D particles[], int numParticles) {
    using math::RandomUtils;
    using math::MathUtils;
    for (int i = 0; i < numParticles; ++i) {
        particles[i].translation.x = RandomUtils::generateGaussianRandomNumber(estimate.translation.x,
                                                                               sigma.translation.x);
        particles[i].translation.y = RandomUtils::generateGaussianRandomNumber(estimate.translation.y,
                                                                               sigma.translation.y);
        particles[i].rotation = MathUtils::normalizeAngle(
                RandomUtils::generateGaussianRandomNumber(estimate.rotation, sigma.rotation));
    }
}

void Resetter::sensorReset(math::Pose2D *particles, int numParticles,
                           const std::vector<GoalPostObservation> &goalPostObservations,
                           const std::vector<FlagObservation> &flagObservations,
                           double resetProbability) {
    int numObservations = goalPostObservations.size() + flagObservations.size();

    if (numObservations < 2 || resetProbability < 1.0e-3)
        return;

    std::vector<int> range(numObservations);
    for (int k = 0; k < numObservations; ++k)
        range[k] = k;

    std::vector<const LandmarkObservation *> landmarkObservations;
    landmarkObservations.reserve(numObservations);
    for (int k = 0; k < goalPostObservations.size(); ++k)
        landmarkObservations.push_back(&goalPostObservations[k]);
    for (int k = 0; k < flagObservations.size(); ++k)
        landmarkObservations.push_back(&flagObservations[k]);

    std::vector<math::Vector2d> knownPositions;
    knownPositions.reserve(numObservations);
    for (int k = 0; k < goalPostObservations.size(); ++k)
        knownPositions.push_back(fieldDescription.getGoalPostKnownPosition(goalPostObservations[k].getGoalPostType()));
    for (int k = 0; k < flagObservations.size(); ++k)
        knownPositions.push_back(fieldDescription.getFlagKnownPosition(flagObservations[k].getFlagType()));

    int i = 0, j = 1;
    for (int p = 0; p < numParticles; ++p) {
        if (math::RandomUtils::generateUniformRandomNumber(0.0, 1.0) < resetProbability) {
            if (numObservations > 2) {
                math::RandomUtils::shuffle(range);
                i = range[0];
                j = range[1];
            }
            resetWithTwoLandmarks(*landmarkObservations[i], *landmarkObservations[j], knownPositions[i], knownPositions[j],
                                  particles[p]);
        }
    }
}

void Resetter::resetWithTwoLandmarks(const LandmarkObservation &observation1,
                                     const LandmarkObservation &observation2,
                                     const math::Vector2<double> &knownPosition1,
                                     const math::Vector2<double> &knownPosition2, math::Pose2D &newParticle) {
    double distance1 = observation1.getDistance() + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaDistance);
    double distance2 = observation2.getDistance() + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaDistance);
    double bearing1 = observation1.getBearing() + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaBearing);
    double bearing2 = observation2.getBearing() + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaBearing);

    math::Vector2<double> intersection1;
    math::Vector2<double> intersection2;
    bool success = computeTwoCirclesIntersection(knownPosition1, distance1,
                                                 knownPosition2, distance2,
                                                 intersection1, intersection2);
    if (!success)
        return;

    bool intersection1WithinField = fieldDescription.isPointWithinSoccerField(intersection1);
    bool intersection2WithinField = fieldDescription.isPointWithinSoccerField(intersection2);
    if (!intersection1WithinField && !intersection2WithinField) {
        return;
    } else if (intersection1WithinField && !intersection2WithinField) {
        newParticle.translation = intersection1;
    } else if (!intersection1WithinField && intersection2WithinField) {
        newParticle.translation = intersection2;
    } else {
        int i = math::RandomUtils::generateIntegerRandomNumber(0, 1);
        if (i == 0)
            newParticle.translation = intersection1;
        else
            newParticle.translation = intersection2;
    }

    math::Vector2d difference1 = knownPosition1 - newParticle.translation;
    math::Vector2d difference2 = knownPosition2 - newParticle.translation;
    double psi1 = math::MathUtils::normalizeAngle(difference1.angle() - bearing1);
    double psi2 = math::MathUtils::normalizeAngle(difference2.angle() - bearing2);

    double sumSine = sin(psi1) + sin(psi2);
    double sumCossine = cos(psi1) + cos(psi2);

    newParticle.rotation = std::atan2(sumSine, sumCossine);
}

bool
Resetter::computeTwoCirclesIntersection(const math::Vector2<double> &center1, double radius1,
                                        const math::Vector2<double> &center2,
                                        double radius2, math::Vector2<double> &intersection1,
                                        math::Vector2<double> &intersection2) {
    double r0 = radius1;
    double r1 = radius2;
    double d = (center1 - center2).abs();

    if (d > r0 + r1)
        return false;
    if (d < fabs(r0 - r1))
        return false;

    double a = (r0 * r0 - r1 * r1 + d * d) / (2 * d);
    double h = sqrt(r0 * r0 - a * a);

    math::Vector2<double> p0 = center1;
    math::Vector2<double> p1 = center2;
    math::Vector2<double> p2 = p0 + (p1 - p0) * (a / d);

    intersection1.x = p2.x + (h / d) * (p1.y - p0.y);
    intersection1.y = p2.y - (h / d) * (p1.x - p0.x);

    intersection2.x = p2.x - (h / d) * (p1.y - p0.y);
    intersection2.y = p2.y + (h / d) * (p1.x - p0.x);

    return true;
}
