//
// Created by mmaximo on 5/4/18.
//

#include <math/MathUtils.h>
#include "OdometryModel.h"
#include "math/RandomUtils.h"

OdometryModel::OdometryModel(double sigmaVx, double sigmaVy, double sigmaVpsi) : sigmaVx(sigmaVx), sigmaVy(sigmaVy),
                                                                                 sigmaVpsi(sigmaVpsi) {
}

math::Pose2D OdometryModel::predict(double elapsedTime, const math::Pose2D &pose, const math::Pose2D &velocity) {
    static math::Pose2D newPose;
    static math::Pose2D noisyVelocity;
    noisyVelocity.translation.x = math::RandomUtils::generateGaussianRandomNumber(velocity.translation.x, sigmaVx);
    noisyVelocity.translation.y = math::RandomUtils::generateGaussianRandomNumber(velocity.translation.y, sigmaVy);
    noisyVelocity.rotation = math::RandomUtils::generateGaussianRandomNumber(velocity.rotation, sigmaVpsi);
    if (std::abs(noisyVelocity.rotation) < 1.0e-3) {
        newPose.translation.x = pose.translation.x + noisyVelocity.translation.x * elapsedTime *
                                                     cos(pose.rotation + noisyVelocity.rotation * elapsedTime / 2.0) -
                                noisyVelocity.translation.y * elapsedTime *
                                sin(pose.rotation + noisyVelocity.rotation * elapsedTime / 2.0);
        newPose.translation.y = pose.translation.y + noisyVelocity.translation.x * elapsedTime *
                                                     sin(pose.rotation + noisyVelocity.rotation * elapsedTime / 2.0) +
                                noisyVelocity.translation.y * elapsedTime *
                                cos(pose.rotation + noisyVelocity.rotation * elapsedTime / 2.0);
    } else {
        newPose.translation.x = pose.translation.x + 2.0 * (noisyVelocity.translation.x / noisyVelocity.rotation) *
                                                     cos(pose.rotation + noisyVelocity.rotation * elapsedTime / 2.0) *
                                                     sin(noisyVelocity.rotation * elapsedTime / 2.0) -
                                2.0 * (noisyVelocity.translation.y / noisyVelocity.rotation) *
                                sin(pose.rotation + noisyVelocity.rotation * elapsedTime / 2.0) *
                                sin(noisyVelocity.rotation * elapsedTime / 2.0);
        newPose.translation.y = pose.translation.y + 2.0 * (noisyVelocity.translation.x / noisyVelocity.rotation) *
                                                     sin(pose.rotation + noisyVelocity.rotation * elapsedTime / 2.0) *
                                                     sin(noisyVelocity.rotation * elapsedTime / 2.0) +
                                2.0 * (noisyVelocity.translation.y / noisyVelocity.rotation) *
                                cos(pose.rotation + noisyVelocity.rotation * elapsedTime / 2.0) *
                                sin(noisyVelocity.rotation * elapsedTime / 2.0);
    }
    newPose.rotation = math::MathUtils::normalizeAngle(pose.rotation + noisyVelocity.rotation * elapsedTime);
    return newPose;
}
