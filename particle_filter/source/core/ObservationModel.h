//
// Created by mmaximo on 5/4/18.
//

#ifndef PARTICLE_FILTER_OBSERVATIONMODEL_H
#define PARTICLE_FILTER_OBSERVATIONMODEL_H

#include "math/Pose2D.h"
#include "LandmarkObservation.h"
#include "FieldDescription.h"
#include <vector>

/**
 * Represents a robot soccer observation model.
 */
class ObservationModel {
public:
    /**
     * Constructs a robot soccer observation model.
     *
     * @param fieldDescription description of the soccer field.
     * @param sigmaDistance standard deviation of distance observations.
     * @param sigmaBearing standard deviation of bearing observations.
     */
    ObservationModel(FieldDescription &fieldDescription, double sigmaDistance, double sigmaBearing);

    /**
     * Computes the log probability of a landmark observation.
     *
     * @param estimate estimate of the robot pose.
     * @param observation the landmark observation.
     * @param knownPosition known position of the landmark in the soccer field.
     * @return log probability of the observation.
     */
    double computeLogProbabilityFromOneLandmark(const math::Pose2D &estimate, const LandmarkObservation &observation,
                                             const math::Vector2<double> &knownPosition);

    /**
     * Computes probability of landmark-based observations.
     *
     * @param estimate estimate of the robot pose.
     * @param goalPostObservations goal post observations.
     * @param flagObservations flag observations.
     * @return probability of the observation.
     */
    double computeProbabilityFromLandmarks(const math::Pose2D &estimate, const std::vector<GoalPostObservation> &goalPostObservations,
                                                const std::vector<FlagObservation> &flagObservations);

private:
    FieldDescription &fieldDescription;
    double sigmaDistance;
    double sigmaBearing;
};


#endif //PARTICLE_FILTER_OBSERVATIONMODEL_H
