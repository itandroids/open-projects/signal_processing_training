//
// Created by mmaximo on 5/11/18.
//

#include "Resetter.h"
#include "OdometryModel.h"
#include "math/Pose2D.h"
#include "math/MathUtils.h"
#include <fstream>
#include "ParticleFilterLocalizer.h"
#include "ObservationSimulator.h"
#include "Clock.h"

int main() {
    std::ofstream particlesFile("particles.txt");
    std::ofstream groundTruthFile("ground_truth.txt");
    std::ofstream estimateFile("estimate.txt");
    std::ofstream timeFile("time.txt");
    std::ofstream observationsFile("observations.txt");

    using math::MathUtils;
    using math::Pose2D;

    Clock clock;

    // Parameters
    double T = 1.0 / 30.0;
    double fieldOfView = math::MathUtils::degreesToRadians(60.0);
    ParticleFilterLocalizerParams params = ParticleFilterLocalizerParams::getDefaultParams();
    FieldDescription fieldDescription;

    Pose2D pose(0.0, 0.0, 0.0);
    Pose2D velocity;
    OdometryModel model(params.sigmaVx, params.sigmaVy, params.sigmaVpsi);
    ObservationSimulator simulator(fieldDescription, params.sigmaDistance, params.sigmaBearing, fieldOfView);
    ParticleFilterLocalizer localizer(params, fieldDescription);

    math::Pose2D sigmaPose(MathUtils::degreesToRadians(1.0), 0.1, 0.1);
    localizer.reset(pose, sigmaPose);

    int numSteps = 600;
    std::vector<GoalPostObservation> goalPostsObservations;
    std::vector<FlagObservation> flagObservations;
    for (int k = 0; k < numSteps; ++k) {
        velocity = Pose2D(0.5, 0.5, 0.0); // go forward
        // Propagate actual robot's pose
        pose = model.predict(T, pose, velocity);
        simulator.observe(pose, goalPostsObservations, flagObservations);
        double tic = clock.getCurrentTime();
        localizer.predict(T, velocity);
        localizer.filter(goalPostsObservations, flagObservations);
        double toc = clock.getCurrentTime();
        const math::Pose2D *particles = localizer.getParticles();
        groundTruthFile << pose.translation.x << " " << pose.translation.y << " " << pose.rotation << std::endl;
        estimateFile << localizer.getEstimate().translation.x << " " << localizer.getEstimate().translation.y << " "
                     << localizer.getEstimate().rotation << std::endl;
        for (int i = 0; i < params.numParticles; ++i)
            particlesFile << particles[i].translation.x << " " << particles[i].translation.y << " "
                          << particles[i].rotation << " ";
        particlesFile << std::endl;
        for (int i = 0; i < goalPostsObservations.size(); ++i)
            observationsFile << fieldDescription.getGoalPostKnownPosition(goalPostsObservations[i].getGoalPostType()).x
                             << " "
                             << fieldDescription.getGoalPostKnownPosition(goalPostsObservations[i].getGoalPostType()).y
                             << " ";
        for (int i = 0; i < flagObservations.size(); ++i)
            observationsFile << fieldDescription.getFlagKnownPosition(flagObservations[i].getFlagType()).x << " "
                             << fieldDescription.getFlagKnownPosition(flagObservations[i].getFlagType()).y << " ";
        observationsFile << std::endl;
        timeFile << toc - tic << std::endl;
    }
    groundTruthFile.flush();
    estimateFile.flush();
    particlesFile.flush();
    observationsFile.flush();
    timeFile.flush();
    groundTruthFile.close();
    estimateFile.close();
    particlesFile.close();
    observationsFile.close();
    timeFile.close();
}