//
// Created by mmaximo on 5/5/18.
//

#include <fstream>
#include "math/MathUtils.h"
#include "math/Pose2D.h"
#include "FieldDescription.h"
#include "ObservationModel.h"
#include "ObservationSimulator.h"

int main() {
    std::ofstream particleWeightsFile("particle_and_weights.txt");
    using math::Pose2D;
    using math::MathUtils;
    FieldDescription fieldDescription;
    // Minimum and maximum dimensions of the soccer field
    double minX = -FieldDescription::FIELD_LENGTH / 2.0;
    double maxX = FieldDescription::FIELD_LENGTH / 2.0;
    double minY = -FieldDescription::FIELD_WIDTH / 2.0;
    double maxY = FieldDescription::FIELD_WIDTH / 2.0;
    // Number of points equally spaced in the soccer field in each dimension
    int numPointsHorizontal = 31;
    int numPointsVertical = 21;
    // Space in each dimension
    double dx = FieldDescription::FIELD_LENGTH / (numPointsHorizontal - 1);
    double dy = FieldDescription::FIELD_WIDTH / (numPointsVertical - 1);
    // Noisy used for the model and precise for the simulator
    double sigmaDistanceNoisy = 5.0;
    double sigmaBearingNoisy = MathUtils::degreesToRadians(30.0);
    double sigmaDistancePrecise = 0.01;
    double sigmaBearingPrecise = MathUtils::degreesToRadians(0.1);
    double fieldOfView = MathUtils::degreesToRadians(60.0);
    ObservationModel observationModel(fieldDescription, sigmaDistanceNoisy, sigmaBearingNoisy);
    ObservationSimulator observationSimulator(fieldDescription, sigmaDistancePrecise, sigmaBearingPrecise, fieldOfView);
    Pose2D pose(0.0, 0.0, 0.0);
    std::vector<GoalPostObservation> goalPostObservations;
    std::vector<FlagObservation> flagObservations;
    observationSimulator.observe(pose, goalPostObservations, flagObservations);
    for (int i = 0; i < numPointsHorizontal; ++i) {
        for (int j = 0; j < numPointsVertical; ++j) {
            double x = minX + dx * i;
            double y = minY + dy * j;
            Pose2D particle(0.0, x, y);
            // Computing non-normalized observation probability for each particle
            double probability = observationModel.computeProbabilityFromLandmarks(particle, goalPostObservations, flagObservations);
            particleWeightsFile << x << " " << y << " " << 0.0 << " " << probability << std::endl;
        }
    }
    particleWeightsFile.flush();
    particleWeightsFile.close();
}