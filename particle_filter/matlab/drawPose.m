function drawPose(pose, arrowSize, color)

if nargin < 2
    arrowSize = 0.1;
end

if nargin < 3
    color = 'b';
end

% p1 = pose(1:2);
% dp = arrowSize * [cos(pose(3)), sin(pose(3))];
% quiver(p1(1), p1(2), dp(1), dp(2));
drawArrow(pose(1:2), pose(1:2) + arrowSize * [cos(pose(3)), sin(pose(3))],...
    color);

end