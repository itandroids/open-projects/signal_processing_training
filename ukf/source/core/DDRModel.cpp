//
// Created by mmaximo on 3/24/18.
//

#include "DDRModel.h"

DDRModel::DDRModel(double T, const Eigen::MatrixXd &M, const Eigen::MatrixXd &R) : T(T), M(M), R(R) {
}


Eigen::VectorXd DDRModel::propagate(const Eigen::VectorXd &state, const Eigen::VectorXd &command) const {
    double x = state(0);
    double y = state(1);
    double psi = state(2);
    double v = command(0);
    double w = command(1);
    Eigen::VectorXd newState(3);
    if (fabs(w) < EPSILON) {
        newState(0) = x + v * T * cos(psi + w * T / 2.0);
        newState(1) = y + v * T * sin(psi + w * T / 2.0);
    } else {
        newState(0) = x + 2.0 * (v / w) * cos(psi + w * T / 2.0) * sin(w * T / 2);
        newState(1) = y + 2.0 * (v / w) * sin(psi + w * T / 2.0) * sin(w * T / 2);
    }
    newState(2) = psi + w * T;
    return newState;
}

Eigen::VectorXd DDRModel::observe(const Eigen::VectorXd &state) const {
    Eigen::VectorXd z(3);
    z << state(0), state(1), state(2);
    return z;
}

Eigen::MatrixXd DDRModel::getA(const Eigen::VectorXd &state, const Eigen::VectorXd &command) const {
    double psi = state(2);
    double v = command(0);
    double w = command(1);
    Eigen::MatrixXd A(3, 3);
    if (fabs(w) < EPSILON) {
        A << 1.0, 0.0, -v * T * sin(psi + w * T / 2.0),
                0.0, 1.0, v * T * cos(psi + w * T / 2.0),
                0.0, 0.0, 1.0;
    } else {
        A << 1.0, 0.0, -2.0 * (v / w) * sin(psi + w * T / 2.0) * sin(w * T / 2.0),
                0.0, 1.0, 2.0 * (v / w) * cos(psi + w * T / 2.0) * sin(w * T / 2.0),
                0.0, 0.0, 1.0;
    }
    return A;
}

Eigen::MatrixXd DDRModel::getB(const Eigen::VectorXd &state, const Eigen::VectorXd &command) const {
    double psi = state(2);
    double v = command(0);
    double w = command(1);
    Eigen::MatrixXd B(3, 2);
    if (fabs(w) < EPSILON) {
        B << T * cos(psi + w * T / 2.0), -v * (T * T / 2.0) * sin(psi + w * T / 2.0),
                T * sin(psi + w * T / 2.0), v * (T * T / 2.0) * sin(psi + w * T / 2.0),
                0.0, T;
    } else {
        B << (2.0 / w) * cos(psi + w * T / 2.0) * sin(w * T / 2.0),
                -2.0 * (v / (w * w)) * cos(psi + w * T / 2.0) * sin(w * T / 2.0) + (v / w) * T * cos(psi + w * T),
                (2.0 / w) * sin(psi + w * T / 2.0) * sin(w * T / 2.0),
                -2.0 * (v / (w * w)) * sin(psi + w * T / 2.0) * sin(w * T / 2.0) + (v / w) * T * sin(psi + w * T),
                0.0, T;
    }
    return B;
}

Eigen::MatrixXd DDRModel::getC(const Eigen::VectorXd &state) const {
    Eigen::MatrixXd C(3, 3);
    C << 1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0;
    return C;
}

Eigen::MatrixXd DDRModel::getQ(const Eigen::VectorXd &state, const Eigen::VectorXd &command) const {
    Eigen::MatrixXd B = getB(state, command);
    return B * M * B.transpose();
}

Eigen::MatrixXd DDRModel::getR(const Eigen::VectorXd &state) const {
    return R;
}
