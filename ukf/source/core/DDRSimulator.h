//
// Created by mmaximo on 3/24/18.
//

#ifndef EKF_DDRSIMULATOR_H
#define EKF_DDRSIMULATOR_H

#include <memory>
#include <Eigen/Dense>
#include "NonlinearModel.h"
#include "math/Pose2D.h"

/**
 * Represents a simulator for a differential-drive robot.
 */
class DDRSimulator {
public:
    /**
     * Constructs a simulator for a differential-drive robot.
     *
     * @param T sample time.
     * @param sigmaLinearSpeed standard deviation of the linear speed.
     * @param sigmaAngularSpeed standard deviation of the angular speed.
     * @param sigmaCameraCartesian standard deviation of the x and y camera measurements.
     * @param sigmaCameraAngle standard deviation of the angular camera measurment.
     */
    DDRSimulator(double T, double sigmaLinearSpeed, double sigmaAngularSpeed, double sigmaCameraCartesian, double sigmaCameraAngle);

    const math::Pose2D &update(double linearSpeed, double angularSpeed);

    /**
     * Obtains a VSS-like observation of the robot, i.e. the pose of the robot is observed.
     *
     * @return observed pose.
     */
    math::Pose2D observe();

    /**
     * Obtains ground-truth pose of the robot.
     *
     * @return robot's pose.
     */
    const math::Pose2D& getPose() const;

private:
    math::Pose2D pose;
    double T;
    double sigmaLinearSpeed;
    double sigmaAngularSpeed;
    double sigmaCameraCartesian;
    double sigmaCameraAngle;
};


#endif //EKF_DDRSIMULATOR_H
