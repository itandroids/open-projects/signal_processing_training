//
// Created by mmaximo on 3/24/18.
//

#ifndef EKF_NONLINEARMODEL_H
#define EKF_NONLINEARMODEL_H

#include <Eigen/Dense>

class NonlinearModel {
public:
    /**
     * Propagates the state using the motion model.
     *
     * @param state current state.
     * @param command command executed by the robot.
     * @return new state after propagation.
     */
    virtual Eigen::VectorXd propagate(const Eigen::VectorXd &state, const Eigen::VectorXd &command) const = 0;

    /**
     * Computes a observation vector given the current state.
     *
     * @param state current state.
     * @return observation.
     */
    virtual Eigen::VectorXd observe(const Eigen::VectorXd &state) const = 0;

    /**
     * Obtains the jacobian matrix of the motion model with respect to the state.
     *
     * @param state current state.
     * @param command command executed by the robot.
     * @return jacobian matrix.
     */
    virtual Eigen::MatrixXd getA(const Eigen::VectorXd &state, const Eigen::VectorXd &command) const = 0;

    /**
     * Obtains the jacobian matrix of the motion model with respect to the command.
     *
     * @param state current state.
     * @param command command executed by the robot.
     * @return jacobian matrix.
     */
    virtual Eigen::MatrixXd getB(const Eigen::VectorXd &state, const Eigen::VectorXd &command) const = 0;

    /**
     * Obtains the jacobian matrix of the observation model with respect to the state.
     *
     * @param state current state.
     * @return jacobian matrix.
     */
    virtual Eigen::MatrixXd getC(const Eigen::VectorXd &state) const = 0;

    /**
     * Obtains the linearized covariance of the motion model.
     *
     * @param state current state.
     * @param command command executed by the robot.
     * @return linearized covariance of the motion model.
     */
    virtual Eigen::MatrixXd getQ(const Eigen::VectorXd &state, const Eigen::VectorXd &command) const = 0;

    /**
     * Obtains the covariance of the observation model.
     *
     * @param state current state.
     * @return covariance of the observation model.
     */
    virtual Eigen::MatrixXd getR(const Eigen::VectorXd &state) const = 0;

};


#endif //EKF_NONLINEARMODEL_H
