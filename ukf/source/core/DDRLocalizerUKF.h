//
// Created by mmaximo on 3/24/18.
//

#ifndef EKF_DDRLOCALIZER_H
#define EKF_DDRLOCALIZER_H

#include <Eigen/Dense>
#include <math/Pose2D.h>
#include "NonlinearModel.h"

struct UKFParams {
    double alpha;
    double beta;
    double kappa;

    double getLambda(int L);

    void getWeights(int L, double &wm0, double &wc0, double &wmi, double &wci);

    static UKFParams getDefaultUKFParams();
};

struct DDRLocalizerParams {
    double T;
    double sigmaLinearSpeed;
    double sigmaAngularSpeed;
    double sigmaCameraCartesian;
    double sigmaCameraAngle;

    static DDRLocalizerParams getDefaultDDRLocalizerParams();
};

/**
 * Represents a UKF-based localizer for a differential-drive robot.
 */
class DDRLocalizerUKF {
public:
    /**
     * Constructs a UKF-based localizer for a differential-drive robot.
     *
     * @param modelParams DDR model's parameters.
     * @param ukfParams UKF's parameters.
     */
    DDRLocalizerUKF(const DDRLocalizerParams &modelParams, const UKFParams &ukfParams);

    /**
     * Resets the filter's estimate considering given mean and covariance.
     *
     * @param pose estimate's mean.
     * @param covariance estimate's covariance.
     */
    void reset(const math::Pose2D &pose, const Eigen::MatrixXd &covariance);

    /**
     * Executes a prediction step of the localizer.
     *
     * @param linearSpeed robot's linear speed.
     * @param angularSpeed robot's angular speed.
     * @return robot's estimate after the prediction step.
     */
    const math::Pose2D &predict(double linearSpeed, double angularSpeed);

    /**
     * Executes a filtering step of the localizer.
     *
     * @param observation current observation.
     * @return robot's estimate after the filtering step.
     */
    const math::Pose2D &filter(const math::Pose2D &observation);

    /**
     * Obtains current robot's estimate.
     *
     * @return current estimate.
     */
    const Eigen::VectorXd &getState();

    /**
     * Obtains current robot's estimate as pose.
     *
     * @return current estimate as pose.
     */
    const math::Pose2D &getPose();

    /**
     * Obtains estimate's covariance.
     *
     * @return estimate's covariance.
     */
    const Eigen::MatrixXd &getCovariance();

private:
    DDRLocalizerParams modelParams;
    UKFParams ukfParams;
    std::shared_ptr<NonlinearModel> model;
    Eigen::VectorXd estimate;
    Eigen::MatrixXd covariance;
    math::Pose2D pose;
    const int NUM_STATES = 3;
    const int NUM_COMMANDS = 2;
    const int NUM_OBSERVATIONS = 3;
    Eigen::MatrixXd M;
    Eigen::MatrixXd R;

    void drawSigmaPoints(const Eigen::VectorXd &augmentedMean, const Eigen::MatrixXd &augmentedCovariance,
                         Eigen::VectorXd *sigmaPoints, int L);

    void computeStateMomentsFromSP(const Eigen::VectorXd *sigmaPoints, int L, Eigen::VectorXd &mean,
                                   Eigen::MatrixXd &covariance);

    void computeObservationMomentsFromSP(const Eigen::VectorXd *sigmaPoints, int L, Eigen::VectorXd &mean,
                                         Eigen::MatrixXd &covariance);

    void
    computeStateObservationCovarianceFromSP(const Eigen::VectorXd &stateMean, const Eigen::VectorXd &observationMean,
                                            const Eigen::VectorXd *stateSP, const Eigen::VectorXd *observationSP, int L,
                                            Eigen::MatrixXd &covariance);
};


#endif //EKF_DDRLOCALIZER_H
